$( document ).ready(function() {
    $( function () {
        var dateFormat = "mm/dd/yy",
            from = $( "#from" )
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    showOn: "button",
                    buttonImage: "img/date.png",
                    buttonImageOnly: true,
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#to" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                showOn: "button",
                buttonImage: "img/date.png",
                buttonImageOnly: true,
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    //switcher
    $('#grid').on('click', function () {
        $('.list').addClass('grid');
        $('.main-buttons-grid').addClass('active');
        $('.main-buttons-list').removeClass('active');
    });
    $('#list').on('click', function () {
        $('.list').removeClass('grid');
        $('.main-buttons-list').addClass('active');
        $('.main-buttons-grid').removeClass('active');
    });
});
